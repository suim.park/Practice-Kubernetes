# Practice Kubernetes
Kubernetes, often abbreviated as K8s (where "8" stands for the eight letters between the "K" and "s"), is an open-source platform designed to automate the deployment, scaling, and management of containerized applications. Essentially, it provides a framework to run distributed systems resiliently, allowing for scaling and failover for your applications. Kubernetes manages clusters of virtual machines and schedules containers to run on those machines based on the available resources and the requirements of each container. It also handles a wide range of management activities such as load balancing, traffic routing, and monitoring, making it easier for developers to deploy applications without needing to worry about the underlying infrastructure.


## 1. Set up the environment for Kubernetes
This setup will provide you with a functioning local Kubernetes environment suitable for development and testing. If you encounter any issues, make sure your user has the necessary permissions to access the hypervisor and manage virtual machines.


__Step 1: Update and Install Prerequisites__ <br/>
Before installing Minikube, ensure your system is up-to-date and has the necessary dependencies installed, such as curl and wget:
```bash
sudo apt update
sudo apt install curl wget apt-transport-https
```

__Step 2: Install VirtualBox__ <br/>
Minikube requires a hypervisor. You can use VirtualBox(if you are on Linux). For VirtualBox, install it using:
```bash
sudo apt install virtualbox virtualbox-ext-pack
```

__Step 3: Install Minikube__ <br/>
Once the hypervisor is set up, you can download and install Minikube. The recommended method is to download it directly from the official releases:
```bash
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo cp minikube-linux-amd64 /usr/local/bin/minikube
sudo chmod 755 /usr/local/bin/minikube
```

__Step 4:Start Minikube__ <br/>
Now that Minikube is installed, you can start it using:
```bash
minikube start
```
This command will initiate a local Kubernetes cluster. By default, Minikube starts with the VirtualBox driver. If you installed KVM and wish to use it, specify it with the `--driver=kvm2` option:
```bash
minikube start --driver=kvm2
```

__Step 5: Check Minikube Status__ <br/>
After starting Minikube, you can check the status to ensure it is running correctly:
```bash
minikube status
```

## 2. Deploy your project

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sample-web-app
spec:
  replicas: 2
  selector:
    matchLabels:
      app: sample-web-app
  template:
    metadata:
      labels:
        app: sample-web-app
    spec:
      containers:
      - name: sample-web-app
        image: suimpark825/practice-kubernetes:latest
        ports:
        - containerPort: 8080

---
apiVersion: v1
kind: Service
metadata:
  name: rust-web-app-service
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: sample-web-app
```

__How to apply the configuration__
```bash
kubectl apply -f your-yaml-file.yml
kubectl apply -f rust-web-app-deployment.yml # specifically my file name
```

## After the deployment

__Step 1: Check the Service Status__ <br/>
First, check the status of your service to see if an external IP has been assigned. Use this command:
```bash
kubectl get service rust-web-app-service
```

This command will provide output similar to this:
```
NAME                  TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)        AGE
rust-web-app-service  LoadBalancer   10.100.200.1   203.0.113.1      80:31820/TCP   78s
```
Look for the EXTERNAL-IP field. Initially, it might show `<pending>` while the cloud provider provisions an IP address. Once the `EXTERNAL-IP` is assigned (as shown with an example IP `203.0.113.1` in the above output), you can proceed to the next step.

__Step 2: Access the Web Page__ <br/>
Once the EXTERNAL-IP is available, open a web browser and enter the external IP address using http:// followed by the external IP:
```arduino
http://203.0.113.1
```